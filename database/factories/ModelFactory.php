<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function ($faker) {
    $faker = \Faker\Factory::create('en_AU'); //changes the fake rlocal to produce Australian data
    return [
        'firstName' => $faker->firstName,
        'lastName' => $faker->lastName,
        'email' => $faker->email,
        'password' => str_random(10),
        'userType' => 'user',
        'contactPhone' => $faker->phoneNumber,
        'mobilePhone' => $faker->phoneNumber,
        'streetAddress' => $faker->streetAddress,
        'suburb' => $faker->word,
        'city' => $faker->city,
        'state' => $faker->state,
        'postcode' => $faker->postcode,
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\User::class, function ($faker) {
    $faker = \Faker\Factory::create('en_AU'); //changes the fake rlocal to produce Australian data
    return [
        'userId' => rand(1,5),
        'subCategoryId' => rand(1,5),
        'breedId' => rand(1,5),
        'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 5, $max = 1000),
        'description' => $faker->sentence($nbWords = 10),
    ];
});
