<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstName',100);
            $table->string('lastName',100);
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->enum('userType', ['admin','user']);
            $table->string('contactPhone', 20);
            $table->string('mobilePhone', 20);
            $table->string('streetAddress');
            $table->string('suburb',100);
            $table->string('city',100);
            $table->string('state',100);
            $table->string('postcode', 4);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
