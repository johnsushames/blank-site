<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subCategory', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subCatName',50);
            $table->integer('categoryId')->unsigned();
            $table->boolean('hasBreed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subCategory');
    }
}
