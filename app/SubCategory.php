<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    use EloquentTrait;

    protected $fillable = ['subCatName', 'hasBreed'];

    function category()
    {
        return $this->belongsTo('App\Category', 'categoryId');
    }
}
