<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use EloquentTrait;

    protected $fillable = ['order'];

    function listing()
    {
        return $this->belongsTo('App\Listing', 'listingId');
    }

}
