<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use EloquentTrait;

    protected $fillable = ['categoryName'];
}
