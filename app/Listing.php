<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
    use EloquentTrait;

    protected $fillable = ['price', 'description'];

    function user()
    {
        return $this->belongsTo('App\User', 'userId');
    }

    function breed()
    {
        return $this->belongsTo('App\Breed', 'breedId');
    }

    function subCategory()
    {
        return $this->belongsTo('App\SubCategory', 'subCategoryId');
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }
}
